# dp-generator
Custom DP generator

# Setting Up
### Environment Variables
- set CLOUDINARY_URL=cloudinary://your_key:your_secret@your_cloud_name in your `.env` file in your project's root directory
- Install Dependencies: `npm install`/`yarn` depending on the package manager in use
- Run the app locally: 

# Link
Local machine:
```
http://localhost:3000/
```

Production Server:
```
https://cbn-dp.herokuapp.com
```
