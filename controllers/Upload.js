const multer = require('multer');
const cloudinary = require('cloudinary').v2;

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});

let upload = multer({storage: storage}).single('image');

module.exports = (req, res) => {
    upload(req, res, function (err) {
        cloudinary.uploader.upload(
            req.file.path,
            {
                folder: 'anniversary_dps/yadah_four/',
            },
            function(error, result) {
                if (error) {
                    return res.render('error', error);
                }
                req.session.image = result.public_id;
                return res.redirect('/show');
        });
    })
};
