const cloudinary = require('cloudinary').v2;
const config = require('../config');

class RenderImage {
     getImage(req, res){
        let test =  cloudinary.image( req.session.image,
            {
                format: 'png',
                width: 200, height: 200,
                transformation: {crop: 'fill', height: 280, width: 280 , gravity: 'face', radius: 'max'}
            });
       return res.render('show', {image: test});

    }


}

module.exports = new RenderImage();
